//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import Foundation

@MainActor
public struct ___FILEBASENAMEASIDENTIFIER___ {
  /// 컴파일 타임에 필요한 의존성을 선언한 구조체입니다.
  public struct Dependency {
    let someWorker: any SomeWorkable
    let nextSceneBuilder: any NextSceneBuildable
    
    public init(someWorker: any SomeWorkable, nextSceneBuilder: any NextSceneBuildable) {
      self.someWorker = someWorker
      self.nextSceneBuilder = nextSceneBuilder
    }
  }
  
  private let dependency: Dependency
  
  public init(dependency: Dependency) {
    self.dependency = dependency
  }
}

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___SceneBuildable {
  ///  VIP Cycle, 런타임 의존성이 설정된 ViewController 인스턴스를 반환하는 함수입니다.
  /// - Parameter payload: 런타임에 전달받아야 하는 의존성입니다.
  /// - Returns: 런타임 의존성, VIP Cycle이 설정된 ViewController를 반환합니다.
  public func build(with payload: any ___VARIABLE_sceneName___ScenePayloadable) -> any ___VARIABLE_sceneName___ViewControllable {
    let someViewController = self.configureVIPCycle(for: ___VARIABLE_sceneName___ViewController())
    self.setPayload(for: someViewController, with: payload)
    
    return someViewController
  }
}

extension ___FILEBASENAMEASIDENTIFIER___ {
  /// VIP Cycle을 설정합니다.
  /// - Parameter viewController: VIPCycle을 설정할 viewController입니다.
  /// - Returns: VIP Cycle 설정이 완료된 `ViewControllable` 프로토콜을 준수한 `ViewController` 인스턴스를 반환합니다.
  private func configureVIPCycle(for viewController: ___VARIABLE_sceneName___ViewController) -> ___VARIABLE_sceneName___ViewController {
    let interactor = ___VARIABLE_sceneName___Interactor(someWorker: self.dependency.someWorker)
    let presenter = ___VARIABLE_sceneName___Presenter()
    let router = ___VARIABLE_sceneName___Router(nextSceneBuilder: self.dependency.nextSceneBuilder)
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
    
    return viewController
  }
  
  /// ViewController에 런타임 파라미터 `payload`를 설정합니다.
  /// - Parameters:
  ///   - viewController: 런타임 의존성을 설정할 ViewController 객체입니다.
  ///   - payload: 런타임에 전달할 의존성입니다.
  private func setPayload(for viewController: ___VARIABLE_sceneName___ViewController, with payload: any ___VARIABLE_sceneName___ScenePayloadable) {
    // viewController.router?.dataStore?.name = payload.name
  }
}
