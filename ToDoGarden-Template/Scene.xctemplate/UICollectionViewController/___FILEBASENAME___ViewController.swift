//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import UIKit

@MainActor
protocol ___VARIABLE_sceneName___DisplayLogic: AnyObject {
  func displaySomething(viewModel: ___VARIABLE_sceneName___.Something.ViewModel)
}

class ___FILEBASENAMEASIDENTIFIER___: UICollectionViewController, ___VARIABLE_sceneName___ViewControllable {
  
  // MARK: - VIP Properties
  
  var interactor: (any ___VARIABLE_sceneName___BusinessLogic)?
  var router: (any (___VARIABLE_sceneName___RoutingLogic & ___VARIABLE_sceneName___DataPassing))?
  
  // MARK: - Object lifecycle
  
  override init(
    nibName nibNameOrNil: String?,
    bundle nibBundleOrNil: Bundle?
  ) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.doSomething()
  }
}


// MARK: - Confirm display logic protocol

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___DisplayLogic {
  func displaySomething(viewModel: ___VARIABLE_sceneName___.Something.ViewModel) {
    // self.nameTextField.text = viewModel.name
  }
}

// MARK: - Request to interactor

extension ___FILEBASENAMEASIDENTIFIER___ {
  func doSomething() {
    let request = ___VARIABLE_sceneName___.Something.Request()
    self.interactor?.doSomething(request: request)
  }
}
