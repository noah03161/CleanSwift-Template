//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import Foundation

@MainActor
protocol ___VARIABLE_sceneName___RoutingLogic {
  func routeToSomewhere()
}

protocol ___VARIABLE_sceneName___DataPassing {
  var dataStore: ___VARIABLE_sceneName___DataStore? { get }
}

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___DataPassing {
  weak var viewController: ___VARIABLE_sceneName___ViewController?
  var dataStore: (any ___VARIABLE_sceneName___DataStore)?
  private let someSceneBuilder: any SomeSceneBuildable
  
  init(someSceneBuilder: any SomeSceneBuildable) {
    self.someSceneBuilder = someSceneBuilder
  }
}

// MARK: - Routing

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___RoutingLogic {
  func routeToSomewhere() {
    let destinationViewController = self.someSceneBuilder.build(with: SomeScenePayload())
    
    self.viewController?.present(destinationViewController, animated: true)
  }
}

// MARK: - Declare Payload for scene

extension ___FILEBASENAMEASIDENTIFIER___ {
  struct SomeScenePayload: SomeScenePayloadable {
    // var name: String
  }
}

