//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import Foundation

protocol ___VARIABLE_sceneName___DataStore {
  // var name: String { get set }
}

@MainActor
protocol ___VARIABLE_sceneName___BusinessLogic {
  func doSomething(request: ___VARIABLE_sceneName___.Something.Request)
}

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___DataStore {
  // var name: String = ""
  var presenter: (any ___VARIABLE_sceneName___PresentationLogic)?
  private let someWorker: any ___VARIABLE_sceneName___Workable
  
  init(someWorker: any ___VARIABLE_sceneName___Workable) {
    self.someWorker = someWorker
  }
}

// MARK: - Request to worker

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___BusinessLogic {
  func doSomething(request: ___VARIABLE_sceneName___.Something.Request) {
    self.someWorker.doSomeWork()
    
    let response = ___VARIABLE_sceneName___.Something.Response()
    self.presenter?.presentSomething(response: response)
  }
}
