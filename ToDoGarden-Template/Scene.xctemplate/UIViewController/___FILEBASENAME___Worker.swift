//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import Foundation

protocol ___VARIABLE_sceneName___Workable {
  func doSomeWork()
}

struct ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___Workable {
  func doSomeWork() {
  }
}
