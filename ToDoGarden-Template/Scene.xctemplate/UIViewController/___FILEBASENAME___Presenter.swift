//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ToDoGarden. All rights reserved.

import Foundation

@MainActor
protocol ___VARIABLE_sceneName___PresentationLogic {
  func presentSomething(response: ___VARIABLE_sceneName___.Something.Response)
}

class ___FILEBASENAMEASIDENTIFIER___ {
  weak var viewController: (any ___VARIABLE_sceneName___DisplayLogic)?
}

// MARK: - Request to ViewController

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_sceneName___PresentationLogic {
  func presentSomething(response: ___VARIABLE_sceneName___.Something.Response) {
    let viewModel = ___VARIABLE_sceneName___.Something.ViewModel()
    self.viewController?.displaySomething(viewModel: viewModel)
  }
}
