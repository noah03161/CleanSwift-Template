//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.

@testable import ___PROJECTNAMEASIDENTIFIER___
import XCTest

final class ___VARIABLE_sceneName___WorkerTests: XCTestCase {
	// MARK: Subject under test
	
	var sut: ___VARIABLE_sceneName___Worker!
	
	// MARK: Tests
	
	func testSomething() {
		// Given
		
		// When
		
		// Then
	}
}

// MARK: - Test lifecycle

extension ___VARIABLE_sceneName___WorkerTests {
	override func setUp() {
		super.setUp()
		self.setup___VARIABLE_sceneName___Worker()
	}
	
	override func tearDown() {
		super.tearDown()
	}
}

// MARK: - Test setup

extension ___VARIABLE_sceneName___WorkerTests {
	func setup___VARIABLE_sceneName___Worker() {
		self.sut = ___VARIABLE_sceneName___Worker()
	}
}

// MARK: - Test doubles
