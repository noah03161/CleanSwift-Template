//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.

@testable import ___PROJECTNAMEASIDENTIFIER___
import XCTest

final class ___VARIABLE_sceneName___ViewControllerTests: XCTestCase {
	// MARK: Subject under test
	
	var sut: ___VARIABLE_sceneName___ViewController!
	var window: UIWindow!
	
	// MARK: Tests
	
	func testShouldDoSomethingWhenViewIsLoaded() {
		// Given
		let spy = ___VARIABLE_sceneName___BusinessLogicSpy()
		self.sut.interactor = spy
		
		// When
		self.loadView()
		
		// Then
		XCTAssertTrue(spy.doSomethingCalled, "viewDidLoad() should ask the interactor to do something")
	}
	
	func testDisplaySomething() {
		// Given
		let viewModel = ___VARIABLE_sceneName___.Something.ViewModel()
		
		// When
		self.loadView()
		sut.displaySomething(viewModel: viewModel)
		
		// Then
		XCTAssertEqual(sut.nameTextField.text, "", "displaySomething(viewModel:) should update the name text field")
	}
}

// MARK: - Test lifecycle

extension ___VARIABLE_sceneName___ViewControllerTests {
	override func setUp() {
		super.setUp()
		self.window = UIWindow()
		self.setup___VARIABLE_sceneName___ViewController()
	}
	
	override func tearDown() {
		self.window = nil
		super.tearDown()
	}
}

// MARK: Test setup

extension ___VARIABLE_sceneName___ViewControllerTests {
	func setup___VARIABLE_sceneName___ViewController() {
		self.sut = ___VARIABLE_sceneName___ViewController()
	}
	
	func loadView() {
		self.window.addSubview(sut.view)
		RunLoop.current.run(until: Date())
	}
}

// MARK: - Test doubles

extension ___VARIABLE_sceneName___ViewControllerTests {
	final class ___VARIABLE_sceneName___BusinessLogicSpy: ___VARIABLE_sceneName___BusinessLogic {
		var doSomethingCalled = false
		
		func doSomething(request: ___VARIABLE_sceneName___.Something.Request) {
			self.doSomethingCalled = true
		}
	}
}
