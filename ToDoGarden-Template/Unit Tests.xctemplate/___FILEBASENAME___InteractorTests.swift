//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.

@testable import ___PROJECTNAMEASIDENTIFIER___
import XCTest

final class ___VARIABLE_sceneName___InteractorTests: XCTestCase {
	
	// MARK: Subject under test
	
	var sut: ___VARIABLE_sceneName___Interactor!
	
	// MARK: Tests
	
	func testDoSomething() {
		// Given
		let spy = ___VARIABLE_sceneName___PresentationLogicSpy()
		self.sut.presenter = spy
		let request = ___VARIABLE_sceneName___.Something.Request()
		
		// When
		self.sut.doSomething(request: request)
		
		// Then
		XCTAssertTrue(spy.presentSomethingCalled, "doSomething(request:) should ask the presenter to format the result")
	}
}

// MARK: - Test lifecycle

extension ___VARIABLE_sceneName___InteractorTests {
	override func setUp() {
		super.setUp()
		self.setup___VARIABLE_sceneName___Interactor()
	}
	
	override func tearDown() {
		super.tearDown()
	}
}


// MARK: - Test setup

extension ___VARIABLE_sceneName___InteractorTests {
	func setup___VARIABLE_sceneName___Interactor() {
		self.sut = ___VARIABLE_sceneName___Interactor(someWorker: SomeMockWorker())
	}
}

// MARK: - Test doubles

extension ___VARIABLE_sceneName___InteractorTests {
	final class ___VARIABLE_sceneName___PresentationLogicSpy: ___VARIABLE_sceneName___PresentationLogic {
		var presentSomethingCalled = false
		
		func presentSomething(response: ___VARIABLE_sceneName___.Something.Response) {
			self.presentSomethingCalled = true
		}
	}
}
