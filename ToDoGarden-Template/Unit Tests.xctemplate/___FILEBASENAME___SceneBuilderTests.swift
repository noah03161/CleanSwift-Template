//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright (c) ___YEAR___ ___ORGANIZATIONNAME___. All rights reserved.

@testable import ___PROJECTNAMEASIDENTIFIER___
import XCTest

final class ___FILEBASENAMEASIDENTIFIER___: XCTestCase {
	// MARK: Subject under test
	
	var sut: ___VARIABLE_sceneName___SceneBuilder!
	
	// MARK: Tests
	
	func test_BuildMethod의_반환값이_올바른VIPCycle을_형성하는가() throws {
		// Given
		let payload = ___VARIABLE_sceneName___ScenePayloadableStub(name: "test")
		
		// When
		let someViewController = self.sut.build(with: payload)
		
		// Then
		let viewController = try XCTUnwrap(someViewController as? ___VARIABLE_sceneName___ViewController, "ViewController 언래핑 실패")
		let interactor = try XCTUnwrap(viewController.interactor as? ___VARIABLE_sceneName___Interactor, "Interactor 언래핑 실패")
		XCTAssertTrue(interactor.presenter is ___VARIABLE_sceneName___Presenter, "Presenter 타입 확인 실패")
		XCTAssertTrue(viewController.router is ___VARIABLE_sceneName___Router, "Router 타입 확인 실패")
	}
}

// MARK: - Test lifecycle

extension ___FILEBASENAMEASIDENTIFIER___ {
	override func setUp() {
		super.setUp()
		self.setup___VARIABLE_sceneName___SceneBuilder()
	}
	
	override func tearDown() {
		super.tearDown()
		self.tearDown___VARIABLE_sceneName___SceneBuilder()
	}
	
	func setup___VARIABLE_sceneName___SceneBuilder() {
		self.sut = ___VARIABLE_sceneName___SceneBuilder(
			dependency: .init(
				worker: SomeWorkableStub(),
				someSceneBuilder: NextSceneBuildableStub()
			)
		)
	}
	
	func tearDown___VARIABLE_sceneName___SceneBuilder() {
		self.sut = nil
	}
}

// MARK: - Test doubles

extension ___FILEBASENAMEASIDENTIFIER___ {
	struct ___VARIABLE_sceneName___ScenePayloadableStub: ___VARIABLE_sceneName___ScenePayloadable {
		let name: String
	}
	
	struct SomeWorkableStub: SomeWorkable {
		func doSomeWork() {
			return
		}
	}
	
	struct NextSceneBuildableStub: NextSceneBuildable {
		
	}
}
