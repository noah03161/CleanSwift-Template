# Change Log

## `v0.2.1`

## Unit Test Indentaion 정렬 및 README 문서 추가

- Unit Test 템플릿의 Indentation을 일관되게 정렬하였습니다.
- README 문서에 문제정의, 작업내용, 결과를 추가하였습니다.

## `v0.2.0`

### Swift Package template 지원, public 접근제어자 추가

- ToDoGarden 프로젝트를 Swift Pakcage를 이용해 관리하는 것으로 결정하였으므로, ToDoGarden template을 Swift Package에서 사용하기 위한 설정을 추가하였습니다.  
- 외부 모듈에 공개되어야 하는 인터페이스에 public 접근제어자를 추가하였습니다.

## `v0.0.1`

### 템플릿을 이용해 Scene을 생성할 때 아래의 컴포넌트가 생성되도록 하였습니다 :

ViewController
Interactor
Worker
Presenter
Router
Models
~ViewControllable
~SceneBuilder
~SceneBuildable

앞에 ~가 붙은 타입은 다른 모듈에서 해당 컴포넌트의 소스코드 의존성을 끊어내고, 구현부가 아닌 인터페이스에 의존하도록 하기 위해 만들어진 컴포넌트입니다.

### SceneBuilder를 이용해 생성한 ViewController 인스턴스 VIP Cycle 검증

- SceneBuilder를 이용해 생성한 ViewController 인스턴스의 VIP Cycle이 올바르게 설정되었는지 확인하는 단위 테스트 코드 템플릿을 추가하였습니다.
